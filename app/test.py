import unittest

from app import app


class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_hello_world(self):
        """Check if hello world it's OK and return 'world'"""
        r = self.app.get('/helloworld')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json['hello'], "world")

if __name__ == '__main__':
    unittest.main()
